class Route

  def initialize
    $routes ||= {}
  end

  def create
    puts "Please enter route's name you want to create:"
    @route_name = gets.chomp

    $routes[@route_name] = ""

    puts $routes
  end

  def delete
    puts "Please enter routes's name you want delete:"
    @route_del = gets.chomp

    $routes.delete(@route_del)

    puts "#{@route_del} deleted! \n" "All routes: #{$routes}"
  end

  def add_first_and_last
    puts "On which route you need to add first or last station?"
    input_route = gets.chomp
    if $routes.key?(input_route)
      puts "Wich station you need to add?"
      input_station = gets.chomp
      if $stations.key?(input_station)
        puts "To add start station type 1. To add last station type 2."
        first_or_last = gets.chomp.to_i
        if first_or_last == 1
          if $routes[input_route] == ""
            $routes[input_route] = []
            $routes[input_route].insert(0, input_station)
            puts "Station #{input_station} added to the route #{input_route} as first station! \n"
          elsif $routes[input_route] == []
            $routes[input_route].insert(0, input_wich_station)
            puts "Station #{input_station} added to the route #{input_route} as first station! \n"
          else
            $routes[input_route].insert(0, input_station)
          end
        elsif first_or_last == 2
          if $routes[input_route] == ""
            $routes[input_route] = []
            $routes[input_route].insert(-1, input_station)
            puts "Station #{input_station} added to the route #{input_route}! \n"
          elsif $routes[input_route] == []
            $routes[input_route].insert(-1, input_station)
            puts "Station #{input_station} added to the route #{input_route}! \n"
          else
            $routes[input_route].insert(-1, input_station)
          end
        else
          puts "Type 1 or 2"
        end
      else
        "No station with this name"
      end
    else
      "No station with this name"
    end
  end

  def show
    puts "Wich route you want to see?"
    route_show = gets.chomp

    if $routes.key?(route_show)
      puts $routes[route_show]
    else
      puts "No route with this name"
    end
  end

  def add_station

    puts "On which route station need to be added?"
    input_wich_route = gets.chomp

    if $routes.key?(input_wich_route)
    puts "Wich station you want to add?"
    input_wich_station = gets.chomp
      if $stations.key?(input_wich_station)
        if $routes[input_wich_route] == ""
          $routes[input_wich_route] = []
          $routes[input_wich_route].push(input_wich_station)
          puts "Station #{input_wich_station} added to the route #{input_wich_route}! \n"
        elsif $routes[input_wich_route] == []
        $routes[input_wich_route].push(input_wich_station)
          puts "Station #{input_wich_station} added to the route #{input_wich_route}! \n"
        else
          $routes[input_wich_route].push(input_wich_station)
        end

      else
        puts "No station with this name"
      end

    else

      puts "No route with this name"

      end

  end

  def del_station

    puts "On which route you need to delete station?"
    route_select = gets.chomp

    if $routes.key?(route_select)
      puts "Please enter station's name you want delete from route:"
      station_delete_from_route = gets.chomp

      if $stations.key?(station_delete_from_route)
        $routes[route_select].delete(station_delete_from_route)
        puts "#{station_delete_from_route} deleted from route #{route_select}! \n #{$routes}"
      else
        puts "No station with this name"
        end

    else
      puts "No route with this name"
    end


  end

end