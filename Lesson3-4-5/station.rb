class Station

  def initialize
    $stations ||= {}
  end

  def create

    puts "Please enter station's name you want to create:"
    @station_name = gets.chomp

    $stations[@station_name] = ""

    puts $stations

  end

  def delete

    puts "Please enter station's name you want delete:"
    @station_delete = gets.chomp

    $stations.delete(@station_delete)

    puts "#{@station_delete} deleted! \n" "All stations: #{$stations}"

  end

  def show_stations
    $stations.each_key{|k| puts k }
  end

  def get_trains
    puts "From which station you want to see list of trains? Type ALL to see all stations and trains on it."
    @user_request = gets.chomp

    if @user_request == "ALL"
      for station in $stations
        puts station
      end
    elsif
    puts "#{$stations[@user_request]}"
    else
      puts "There are no trains on this station"
    end
  end

  def train_type
    puts "From which station you want to count trains type? Type ALL to see train types on all stations."
    @trains_type_request_station = gets.chomp


    if @trains_type_request_station == "ALL"
      puts "Cargo trains on stations: #{$stations.to_s.scan(/Cargo/).count} \n
          Passenger trains on stations: #{$stations.to_s.scan(/Passenger/).count}"
    elsif $stations[@trains_type_request_station] == nil
      puts "The are no this station"
    else
      puts "Cargo trains on this station is: #{$stations[@trains_type_request_station].to_s.scan(/Cargo/).count} \n
            Passenger trains on this station is: #{$stations[@trains_type_request_station].to_s.scan(/Passenger/).count}"
      #$stations[@trains_type_request_station].to_s.split(' ').count('Passenger')
    end
  end

  def train_departure

    puts "You see all stations and trains on it: #{$stations}"
    puts "Select the station and specify the train number to departure from stations"
    puts "Which station you want to select?"
    @trains_station_departure = gets.chomp

    if $stations[@trains_station_departure] == [] || $stations[@trains_station_departure] == ""
      puts "No trains on this station"
    elsif $stations[@trains_station_departure].nil?
      puts "No station with this name"
    else
      puts "Which train you want to departure from station? Enter ordinal number"
      @ordinal_num = gets.chomp.to_i

      if $stations[@trains_station_departure].size < @ordinal_num
        puts "There are no train with this ordinal number"
      else
        $stations[@trains_station_departure].delete_at(@ordinal_num - 1)
        puts "Train with ordinal number #{@ordinal_num} was deleted from station #{@trains_station_departure}.\n
              Trains on the station now: #{$stations[@trains_station_departure]}"
      end

    end

  end

end