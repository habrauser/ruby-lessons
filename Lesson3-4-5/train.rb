class Train

  attr_accessor :speed
  attr_accessor :train_type
  attr_accessor :carriage

  def initialize(speed = 0, carriage = 0, train_type = "Not selected yet")
    @speed = speed
    @train_type = train_type
    @carriage = carriage
    #$trains_on_station = {:train_type => [], :carriage => [] }
  end

  def stop
    self.speed = 0
    puts "Train is stoped. Speed: #{self.speed}"
  end

  def go
    self.speed = 30
    puts "Now train speed is #{self.speed}"
  end

  def carriage_attach

    if self.speed == 0
      self.carriage += 1
    else
      puts "You can't attach railway carriage. Train is moving."
    end

    puts "Carriage now is #{self.carriage}"

  end

  def carriage_unhook

    if self.carriage == 0
      puts "There is nothing to unhook"
    elsif self.speed == 0
      self.carriage -= 1
    else
      puts "You can't unhook railway carriage. Train is moving."
    end

    puts "Carriage now is #{self.carriage}"

  end

  def type_select
    @carriage = 0
    puts "Choose train type"
    puts "For passenger enter 1 \nFor cargo enter 2"
    self.train_type = gets.to_i

    if self.train_type == 1
      self.train_type = "Passenger train"
    elsif self.train_type == 2
      self.train_type = "Cargo train"
    else
      puts "Train type not selected"
    end

    puts "Now train type is #{self.train_type}"


  end

  def go_to_station

    @type_and_carriage = [@train_type, @carriage]

    puts "On which station train needs to go?"
    input_wich_station = gets.chomp

    if @train_type != "Not selected yet"

      if $stations.key?(input_wich_station)

        if $stations[input_wich_station] == ""
          $stations[input_wich_station] = []
          $stations[input_wich_station].push(@type_and_carriage)
          puts "Train added to the station #{input_wich_station}! \n" "Type: #{@train_type}, Carriage: #{@carriage}"
        elsif
        $stations[input_wich_station].push(@type_and_carriage)
          puts "Train added to the station #{input_wich_station}! \n" "Type: #{@train_type}, Carriage: #{@carriage}"
        end

      else
        puts "This station are not in list"
      end

    else
      puts "Please select train type and try again"

    end

  end

  def return_to_depot
    #id поезду присвоить, в массив под 2 индексом и управляя им возвращать поезда в депо
  end

end