require_relative 'train'
require_relative 'train-passenger'
require_relative 'train-cargo'
require_relative 'route'
require_relative 'station'

train = Train.new
passengertrain = PassengerTrain.new
cargotrain = CargoTrain.new
station = Station.new
route = Route.new

main_menu = "This is the Railway Station application. \n
      Type 'train' to open trains menu. \n
      Type 'station' to open stations menu. \n
      Type 'route' to open routes menu."

train_menu = "Choose what you want to do:\n
          1. Create train\n
          2. Show selected type\n
          3. Attach carriage\n
          4. Unhook carriage\n
          5. Start train\n
          6. Stop train\n
          7. Move train to station\n\n
          To exit press enter button"

station_menu = "Choose want you want to do:\n
                1. Create station\n
                2. Delete station\n
                3. Show stations\n
                4. Show trains from station\n
                5. Count trains by type on station\n
                6. Departure train from the station\n
                To exit press enter button"

route_menu = "Choose want you want to do:\n
              1. Create route\n
              2. Delete route\n
              3. Add first or last station on route\n
              4. Simply add station on the route\n
              5. Delete staton from the route\n
              6. Show route\n
              To exit press enter button"



while main_menu do

  puts main_menu
  user_do = gets.chomp

case user_do

when "train"
  while train_menu do
  puts train_menu
  user_do_train = gets.chomp
  case user_do_train
  when "1"
    train.type_select
  when "2"
    puts train.train_type
  when "3"
    train.carriage_attach
  when "4"
    train.carriage_unhook
  when "5"
    train.go
  when "6"
    train.stop
  when "7"
    train.go_to_station
  else
    if user_do_train != ""
    puts "No menu of this type"
    end
  end

    break if user_do_train == ""

  end

when "station"
  while station_menu do
  puts station_menu
  user_do_station = gets.chomp
  case user_do_station
  when "1"
    station.create
  when "2"
    station.delete
  when "3"
    station.show_stations
  when "4"
    station.get_trains
  when "5"
    station.train_type
  when "6"
    station.train_departure
  else
    if user_do_station != ""
      puts "No menu of this type"
    end
  end

  break if user_do_station == ""
  end

when "route"
  while route_menu do
  puts route_menu
  user_do_route = gets.chomp

  case user_do_route
  when "1"
    route.create
  when "2"
    route.delete
  when "3"
    route.add_first_and_last
  when "4"
    route.add_station
  when "5"
    route.del_station
  when "6"
    route.show
  else
    if user_do_route != ""
      puts "No menu of this type"
    end
  end

  break if user_do_route == ""
  end

else
  redo

end


end