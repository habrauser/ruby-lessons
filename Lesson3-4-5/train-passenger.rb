class PassengerTrain < Train

  protected

  def carriage_attach1

    super
    puts "Passenger carriage now is #{self.carriage}"

  end

  private

  def carriage_unhook1

    super
    puts "Passenger carriage now is #{self.carriage}"

  end

end