# Прямоугольный треугольник. Программа запрашивает у пользователя 3 стороны треугольника и определяет, является ли треугольник прямоугольным, используя теорему Пифагора и выводит результат на экран. 
# Также, если треугольник является при этом равнобедренным (т.е. у него равны любые 2 стороны), то дополнитьельно выводится информация о том, что треугольник еще и равнобедренный. 
# Подсказка: чтобы воспользоваться теоремой Пифагора, нужно сначала найти самую длинную сторону (гипотенуза) и сравнить ее значение в квадрате с суммой квадратов двух остальных сторон. 
# Если все 3 стороны равны, то треугольник равнобедренный и равносторонний, но не прямоугольный.

puts "1 side of the triangle"
first = gets.to_i
puts "2 side of the triangle"
second = gets.to_i
puts "3 side of the triangle"
third = gets.to_i

triangle_array = [first.to_f, second.to_f, third.to_f]

if triangle_array.sort[2]**2 == (triangle_array.sort[0]**2) + (triangle_array.sort[1]**2)
puts "Triangle is rectangular" # Basic values for this result is 6,8,10 
else
  puts "Triangle not rectangular"
end

if triangle_array.sort[0] == triangle_array.sort[1] || triangle_array.sort[0] == triangle_array [2] || triangle_array.sort[1] == triangle_array [2] 
  puts "Triangle is also isosceles"
else 
  puts "Triangle is also not isosceles"
end