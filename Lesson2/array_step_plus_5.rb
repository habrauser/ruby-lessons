#Заполнить массив числами от 10 до 100 с шагом 5

#Мое решение
start_cycle = 10
end_cycle = 100
arr = []

while start_cycle < end_cycle do
  start_cycle
  start_cycle += 5
  arr << start_cycle
end

# there are is the difference between p, puts and print
p arr

#Изящное
array = (10..100).to_a
array.delete_if { |x| x % 5 != 0 }
p array