# Сделать хеш, содеращий месяцы и количество дней в месяце. В цикле выводить те месяцы, у которых количество дней ровно 30

calendar = {'Jan' => 31, 'Feb' => 29, 'Mar' => 31, 'Apr' => 30, 'May' => 31, 'Jun' => 30, 'Jul' => 31, 'Aug' => 31, 'Sep' => 30, 'Oct' => 31, 'Nov' => 30, 'Dec' => 31
}


calendar.each do |months, days|
  if days == 30
puts months
end
end


# Решение изящней

months = { 'january' => 31, 'february' => 28, 'march'     => 31,
           'april'   => 30, 'may'      => 31, 'june'      => 30,
           'july'    => 31, 'august'   => 31, 'september' => 30,
           'october' => 31, 'november' => 30, 'december'  => 31 }

months.each { |month, days| puts month if days == 30 }