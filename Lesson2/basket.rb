#Сумма покупок. Пользователь вводит поочередно название товара, цену за единицу и кол-во купленного товара (может быть нецелым числом). 
#Пользователь может ввести произвольное кол-во товаров до тех пор, пока не введет "стоп" в качестве названия товара. На основе введенных данных требуетеся:
#Заполнить и вывести на экран хеш, ключами которого являются названия товаров, а значением - вложенный хеш, содержащий цену за единицу товара и кол-во купленного товара. Также вывести итоговую сумму за каждый товар.
#Вычислить и вывести на экран итоговую сумму всех покупок в "корзине".

def user_request


#defined calculation variables
basket = {}
sum = 0
final_sum = 0

 
loop { 
#request
puts "Please enter product you want or type stop to end"
product = gets.chomp
break if product == 'stop'

puts "Please enter price"
price = gets.chomp.to_f

puts "Please enter quantity"
quantity = gets.chomp.to_i

sum = price * quantity
basket[product] = {price => quantity}
final_sum += sum

#output
puts "Amount for #{product} is = #{sum} \n"
puts "Your #{basket} \n"
puts "Final amount for all = #{final_sum}"
}


end

puts user_request