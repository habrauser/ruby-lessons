i = [1,2,3,4,5,6,7]
l = i.reverse

sum = [i, l].transpose.map {|x| x.reduce(:+)}

p "i: #{i}" 
p "l: #{l}"
p "i+l: #{sum}" 

sum_i = [i, i].transpose.map {|x| x.reduce(:+)}

p "i+i: #{sum_i}"

before_transform = sum_i.each do |item|
  if item < 9
    print "#{item} " 
  end 
end 

transform =  sum_i.each do |item|
    if item > 9
    print item.to_s.insert(1, '+').split('+')[0].to_i
    print "+"
    print "#{item.to_s.insert(1, '+').split('+')[-1].to_i} "
  end
end
  
puts "\n"
  
  transform2 =  sum_i.each do |item|
    if item < 9
      print "#{item} "
    elsif item > 9
    a = item.to_s.insert(1, '+').split('+')[0].to_i
    b = item.to_s.insert(1, '+').split('+')[-1].to_i
    print "#{a + b} "
  end
end