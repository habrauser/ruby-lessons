#Lambda и Proc 
# ->() == lambda 
# -> == lambda
# def one - lambda не остановится на 1 return 
# def two - Proc остановится после первого return 

def one 
  lambda() {return 1}.call 
    return 2
end 

def two 
  Proc.new {return 1}.call 
  return 2 
end

print one, two 
