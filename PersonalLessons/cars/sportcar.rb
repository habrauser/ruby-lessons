class Sportcar < Car

  def start_engine
    super #вызывает из родительского класса метод
    puts "Wrooom!"
  end

  protected

  def initial_rpm
    1000
  end

end