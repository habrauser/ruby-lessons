require_relative 'car'
require_relative 'truck'
require_relative 'sportcar'

# require '' RoR

#require_relative Ruby


car = Car.new
car.start_engine

puts "Car RPM: #{car.current_rpm}"

truck = Truck.new
truck.start_engine

puts "Truck RPM: #{truck.current_rpm}"

sportcar = Sportcar.new
sportcar.start_engine

puts "Sport Car RPM: #{sportcar.current_rpm}"