#what map does do in ruby:
#[1, 2, 3].map { |n| n * n } # output=> [1, 4, 9]

#what split does do in ruby:
#" now's  the time".split        #=> ["now's", "the", "time"]

#map(&:to_i)
#array.map(&:to_i)
#This invokes .map on array, and for each element in the array, returns the result of calling to_i on that element.

puts "Hi! With this programm you'll get an array wich separates result by odd and even nums. \n \n Please enter an array nums via comma(Example:1,2,3):"

array = gets.chomp.split(/,/).map(&:to_i)

puts "Even nums in array: #{array.select { |x| x.even? } } \n"

puts "Odd nums in array: #{array.select { |x| x.odd? } } \n"

puts "Complete array is: #{array}"